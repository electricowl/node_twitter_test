module.exports = {

  twitter: {
    consumer_key:        'HzIGmRle5gGLYuPuugMngAaax',
    consumer_secret:     '91JNjwlriFGjVTY7eUhW80ONZeJXVYXngFVJ2hS0yCsZkhHAtY',
    access_token_key:    '120521974-l1TEIV1srfkfyqAHEdNrqJc4Csna4pTQoS4GcPSt',
    access_token_secret: 'loeU3TLzD5oT1QxFyWbsAYZyhETJy0WsjsbKAjyxEd61p',
  },

  eventgroups: [
    "4d483d20ee59a7c1f5661c6a"  //Sport
  ],

  database: {
    host:     'localhost',
    port:     27011,
    poolSize: 1
  },

  news: {
    host: 'api1.internal.fanatix.com',
    port: 7900
  },

  accounts: [{
    id:          '375466790',
    name:        'Stadium Astro',
    account:     'stadiumastro',
    source:      'astro-tweet',
    eventgroups: [ '4d483d1fee59a7c1f5661c00' ]
  },{
    id:          '16824146',
    name:        'Empire of the Kop',
    account:     'empireofthekop',
    source:      'eotk-tweet',
    eventgroups: [ '4d483d1fee59a7c1f5661c00' ],
    attractions: [ '4d10fe7feb3eccefab04308e' ]
  },{
    id:          '16542390',
    name:        'ESPNCricInfo',
    account:     'ESPNcricinfo',
    source:      'espncricinfo-tweet',
    eventgroups: [ '4d483d1fee59a7c1f5661c15' ]
  },{
    id:          '262661372',
    name:        'Fanatix',
    account:     'TheRealFanatix',
    source:      'fanatix-tweet',
    eventgroups: [ '4d483d20ee59a7c1f5661c6a' ]
  },{
    id:          '816871884',
    name:        'Football Direct',
    account:     'fdnfootballnews',
    source:      'sportsdirect-tweet',
    eventgroups: [ '4d483d20ee59a7c1f5661c6a' ]
  },{
    id:          '467781580',
    name:        'Football Espana',
    account:     'footballespana_',
    source:      'footballespana-tweet',
    eventgroups: [ '4d483d1fee59a7c1f5661c00' ]
  },{
    id:          '268002142',
    name:        'Super Rugby',
    account:     'SuperRugby',
    source:      'superrugby-tweet',
    eventgroups: [ '4d483d1fee59a7c1f5661c08', '4f87dd8ac5fe3bb54541df43' ]
  },{
    id:          '407360976',
    name:        'TRC',
    account:     'SanzarTRC',
    source:      'rugby-championship-tweet',
    eventgroups: [ '4d483d1fee59a7c1f5661c08', '51117407034c7d7c48000019', '4e2628b4fb2ae8ba3035ad64' ]
  }]
};   
