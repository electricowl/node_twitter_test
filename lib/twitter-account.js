TwitterAccount = function( id, screen_name, source )
{
  this.id = id;
  this.screen_name = screen_name;
  this.source = source || 'tweet';
  this.attractions = [];
  this.eventgroups = [];
}

TwitterAccount.prototype.addAttractions = function( attractions )
{
  for( idx in attractions )
    if( this.attractions.indexOf( attractions[ idx ] ) < 0 )
      this.attractions.push( attractions[ idx ] ); 
};
TwitterAccount.prototype.addEventgroups = function( eventgroups )
{
  for( idx in eventgroups )
    if( this.eventgroups.indexOf( eventgroups[ idx ] ) < 0 )
      this.eventgroups.push( eventgroups[ idx ] ); 
};

module.exports = TwitterAccount;
