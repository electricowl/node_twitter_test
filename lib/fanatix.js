var Db         = require('mongodb').Db,
    Connection = require('mongodb').Connection,
    Server     = require('mongodb').Server,
    BSON       = require('mongodb').BSON,
    ObjectID   = require('mongodb').ObjectID;


Fanatix = function( host, port, poolSize, callback )
{
  var server = new Server(host, port, {auto_reconnect: true, poolSize: poolSize}, {});
  var self = this;
  this.db = new Db('fanatix', server, {w: 1} );
  this.db.open( callback ? function() { callback( self ); } : function(){} );
};


Fanatix.prototype.getAttractionsByEventgroups = function( eventgroups, fields, limit, offset, callback )
{
  var eventgroupIds = eventgroups.map(function(eventgroup){
    return new ObjectID( eventgroup );
  });

  var query = { 'eventgroups._id': { $in: eventgroupIds } };

  this.find( "attractions", query, { fields: fields, limit: limit, skip: offset }, function( error, cursor ) {

    cursor.toArray( function( error, documents ) {

      if( callback ) {
        callback( error, documents );
        }
    });
  });
};


Fanatix.prototype.getCollection = function( collectionName, callback)
{

  this.db.collection(collectionName, function(error, package_collection) {

    if( error ) {
      callback(error);
    } else {
      callback(null, package_collection);
    }
    
  });
};

Fanatix.prototype.findAll = function(collectionName, callback)
{

  this.getCollection( collectionName, function(error, package_collection) {

    if( error ) {
      callback(error);
   } else {
      package_collection.find().toArray(function(error, documents) {
        if( error ) {
          callback(error);
       } else {
          callback(null, documents);
      }
      });
  }
  
  });
};

Fanatix.prototype.insert = function( collectionName, document, callback)
{
  this.getCollection( collectionName, function(error, package_collection) {
    if( error ) {
      callback(error);
   } 
   else {
      package_collection.insert(document, function() {
        if( callback )
          callback(null, document);
      });
    }
  });
};

Fanatix.prototype.update = function( collectionName, query, document, callback )
{
  this.getCollection( collectionName, function(error, package_collection) {
    if( error ) {
      callback(error);
    }
    else {
      package_collection.update(query, document, {}, function() {
        callback(null, document);
      });
    }
  });
};

Fanatix.prototype.findOne = function( collectionName, query, options, callback)
{
  this.getCollection( collectionName, function(error, package_collection) {

    if( error ) {
      callback(error);
   } else {
      package_collection.findOne( query, options, function(error, document) {

        if( error )
          callback(error);
        else
          callback(null, document);
      });
  }
  });
};

Fanatix.prototype.find = function( collectionName, query, options, callback)
{

  this.getCollection( collectionName, function(error, package_collection) {

    if( error ) {
      callback(error);
  }
    else {
      package_collection.find( query, options, function(error, document) {
          if( error )
            callback(error);
          else
            callback(null, document);
        });
    }
    });
};

module.exports = Fanatix;

