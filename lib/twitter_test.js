/*
 * twitter_test
 * https://github.com/root/node_twitter_test
 *
 * Copyright (c) 2014 Ian Lewis
 * Licensed under the MIT license.
 */

'use strict';

exports.awesome = function() {
  return 'awesome';
};
