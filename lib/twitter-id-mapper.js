TwitterIDMapper = function( fanatix, callback ){
  this.fanatix = fanatix;
  this.accountMap = {};

  this.loadMap(callback);
  var self = this;

  setInterval( function() { self.loadMap(function(){}); }, 3600000 ); // reload once per hour
};

TwitterIDMapper.prototype.getAccountId = function( name, callback ){
  return this.accountMap[ name ] ;
};

TwitterIDMapper.prototype.loadMap = function( callback ){
  var self = this;

  self.accountMap = [];

  console.log( "Loading the account map..." );

  this.fanatix.findAll( 'twitterAccounts', function( error, accounts ){
    if( ! error && accounts && accounts.length > 1 )
      accounts.forEach(function(account){
        self.accountMap[ account._id ] = account.twitterid;
      });   

    callback();
  });
};

module.exports = TwitterIDMapper;

