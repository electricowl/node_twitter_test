/**
 * This one only does one twitter account per attraction
 */
var config           = require( '../config/app' ),
    Fanatix          = require( '../lib/fanatix' ),
    TwitterAccount   = require( '../lib/twitter-account' ),
    TwitterIDManager = require( '../lib/twitter-id-mapper' ),
    twitter          = require( 'ntwitter' ),
    Restler          = require( 'restler' );


var twitterApi = new twitter({
  consumer_key:        config.twitter.consumer_key,
  consumer_secret:     config.twitter.consumer_secret,
  access_token_key:    config.twitter.access_token_key,
  access_token_secret: config.twitter.access_token_secret
});


var accountMap = {};

if( config.accounts )
{
  config.accounts.forEach( function( account ){
    var acc = new TwitterAccount( account.id, account.account, account.source );

    if( account.attractions )
      acc.addAttractions( account.attractions );

    if( account.eventgroups )
      acc.addEventgroups( account.eventgroups );

   accountMap[ account.id ] = acc;
  });

  console.log( "Added " + config.accounts.length + " fixed accounts" );
}


console.log( "Connecting to db..." );
var fanatixApi = new Fanatix( config.database.host, config.database.port, config.database.poolSize, function(){

  console.log( "Loading Twitter ID mapper..." );
  var idMapper = new TwitterIDMapper( fanatixApi, function(){

    console.log( "Loading attractions for " + config.eventgroups.length + " eventgroups..." );
    fanatixApi.getAttractionsByEventgroups( config.eventgroups, { 'twitter.accounts': true, 'eventgroups._id': true, name: true }, null, null, function( err, attractions ) {

      console.log( attractions.length + " attractions loaded" );

      attractions.forEach( function( attraction ){

        if( ! attraction.twitter || ! attraction.twitter.accounts ) return true;

        var attrEventgroups = attraction.eventgroups ? attraction.eventgroups.map(function(eventgroup){ return eventgroup._id.toString(); }) : null;

        attraction.twitter.accounts.forEach(function( twitterAcc ){

          var aid = idMapper.getAccountId( twitterAcc );
          if( ! aid ) return true;

          if( ! accountMap[ aid ] )
            accountMap[ aid ] = new TwitterAccount( aid, twitterAcc );

          accountMap[ aid ].addAttractions( [attraction._id.toString() ]);
           
          if( attrEventgroups )
            accountMap[ aid ].addEventgroups( attrEventgroups );
        });
      });

       console.log( 'Streaming from ' + Object.keys( accountMap ).length + ' mapped twitter accounts' );

       var totalTweets    = 0,
           importedTweets = 0;

       twitterApi.stream(
         'statuses/filter',
         { 'follow': Object.keys( accountMap ) },
         function(stream) {
          stream.on('data', function(tweet) {

            totalTweets++;

            if( totalTweets % 1000 == 0 )
              console.log( "[" + (new Date()).getTime() + "] Stats: " + totalTweets + " total, " + importedTweets + " imported [" + ( Math.round( ( importedTweets / totalTweets ) * 100 ) / 100 ).toFixed( 2 ) + "%]" );
          
            if( ! tweet.user || ! tweet.user.id || tweet.in_reply_to_status_id || tweet.in_reply_to_user_id ) return true;

            var twitterAccount = accountMap[ tweet.user.id_str ];

            if( ! twitterAccount ) return true;

            var extId = 'twitter-' + tweet.id;

            var dateWords = tweet.created_at.split( ' ' );
            var created_at = [ dateWords[ 0 ] + ',', dateWords[ 2 ], dateWords[ 1 ], dateWords[ 5 ], dateWords[ 3 ], dateWords[ 4 ] ].join( ' ' );

            var payload = {
              id: tweet.id,
              id_str: tweet.id_str,
              from_user: tweet.user.screen_name,
              from_user_id: tweet.user.id,
              from_user_id_str: tweet.user.id_str,
              from_user_name: tweet.user.name,
              profile_image_url: tweet.user.profile_image_url,
              text: tweet.text,
              created_at: created_at,
              entities: tweet.entities
            }; 

            var postData = {
              source: twitterAccount.source ? twitterAccount.source : 'tweet',
              country: 'gb',
              language: 'en',
              payload: JSON.stringify( payload ),
              attractions: twitterAccount.attractions,
              eventgroups: twitterAccount.eventgroups
            };

            Restler.post( 'http://' + config.news.host + ':' + config.news.port + '/fanatix-news/v1.0/item/' + extId, {data: postData} ).on('complete', function(data, response) {

            if( data && data.status == 'ok' && data.response )
            {
               console.log( data.response.id + " :: " + tweet.user.name + " :: " +  tweet.text );
               importedTweets++;
            }
          });
        });
      });
    });
  });
});
