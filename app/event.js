var config           = require( '../config/event' ),
    Fanatix          = require( '../lib/fanatix' ),
    TwitterIDManager = require( '../lib/twitter-id-mapper' ),
    twitter          = require( 'ntwitter' ),
    Mysql            = require( 'mysql' ),
    Restler          = require( 'restler' );

var twitterApi = new twitter({
  consumer_key:        config.twitter.consumer_key,
  consumer_secret:     config.twitter.consumer_secret,
  access_token_key:    config.twitter.access_token_key,
  access_token_secret: config.twitter.access_token_secret
});

Array.prototype.getUnique = function(){
   var u = {}, a = [];
   for(var i = 0, l = this.length; i < l; ++i){
      if(u.hasOwnProperty(this[i])) {
         continue;
      }
      a.push(this[i]);
      u[this[i]] = 1;
   }
   return a;
};

var mysqlPool = Mysql.createPool({
  host:            config.tweetDatabase.hostname,
  user:            config.tweetDatabase.username,
  password:        config.tweetDatabase.password,
  database:        config.tweetDatabase.database,
  debug:           false,
  connectionLimit: 20
});



console.log( "Connecting to db..." );
var fanatixApi = new Fanatix( config.database.host, config.database.port, config.database.poolSize, function(){

  console.log( "Loading Twitter ID mapper..." );

  var idMapper = new TwitterIDMapper( fanatixApi, function(){

    console.log( "Loading attractions for " + config.eventgroups.length + " eventgroups..." );
    fanatixApi.getAttractionsByEventgroups( config.eventgroups, { 'twitter.accounts': true, 'eventgroups._id': true, name: true }, null, null, function( err, attractions ) {

      console.log( attractions.length + " attractions loaded" );

      var accountList = []; 
  
      attractions.forEach( function( attraction ){

         if( ! attraction.twitter || ! attraction.twitter.accounts ) return true;

         attraction.twitter.accounts.forEach( function( account ) {
           accountList.push( account );
         });

      });

      var accountNames = accountList.getUnique();
      var accountIds = [];

      accountNames.forEach( function( account ){
          var aid = idMapper.getAccountId( account );
          if( aid ) accountIds.push( aid );
      });

      console.log( 'Streaming from ' + Object.keys( accountIds ).length + ' mapped twitter accounts' );

       var totalTweets    = 0,
           importedTweets = 0;

       twitterApi.stream(
         'statuses/filter',
         { 'follow': accountIds },
         function(stream) {

         stream.on('data', function(tweet) {

           totalTweets++;

           if( totalTweets % 1000 == 0 )
             console.log( "Stats: " + totalTweets + " total, " + importedTweets + " imported [" + ( Math.round( ( importedTweets / totalTweets ) * 100 ) / 100 ).toFixed( 2 ) + "%]" );

            if( tweet.in_reply_to_status_id || tweet.in_reply_to_user_id ) return true;

	    if( ! tweet.user || ! tweet.user.id_str )
                    console.log( tweet );


           var dateWords = tweet.created_at.split( ' ' );
           var created_at = [ dateWords[ 0 ] + ',', dateWords[ 2 ], dateWords[ 1 ], dateWords[ 5 ], dateWords[ 3 ], dateWords[ 4 ] ].join( ' ' );

            var payload = {
             id: tweet.id,
             id_str: tweet.id_str,
             from_user: tweet.user.screen_name,
             from_user_id: tweet.user.id,
             from_user_id_str: tweet.user.id_str,
             from_user_name: tweet.user.name,
             profile_image_url: tweet.user.profile_image_url,
             text: tweet.text,
             created_at: created_at,
             entities: tweet.entities
            };
            //console.log( "Import " + tweet.id_str );
            writeTweet( mysqlPool, tweet.id_str, tweet.user.id_str, JSON.stringify( payload ), function( err, rows ){
              importedTweets++;
            });
        });
      });      
    });
  });
});


function writeTweet( pool, id, accountid, payload, callback )
{
  var self = this;

  try
  {
    var args = [ id, accountid, payload ];
    var query = "INSERT INTO eventtweet ( tweetid, accountid, payload, datetime ) VALUES( ?, ?, ?, NOW() )";


    pool.getConnection(function(err, connection) {

      connection.on('error', function (err) {
        self.logger.error('mysqldb error: ' + err );
        self.logger.error( err.stack );
      });

      if( err )
      {
        callback( err, null, null );
        return true;
      }

      connection.query( query, args, function( err, rows, fields ) {
        if( callback )
          callback( err, rows, fields );

        connection.end();
      });

    });
  }
  catch( err )
  {
    console.log( 'Exception in Query' );
    console.log( err );

    if( callback )
      callback( err, null );
  }
}
