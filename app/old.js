var config           = require( '../config/app' ),
    Fanatix          = require( '../lib/fanatix' ),
    TwitterIDManager = require( '../lib/twitter-id-mapper' ),
    twitter          = require( 'ntwitter' ),
    Restler          = require( 'restler' );

var twitterApi = new twitter({
  consumer_key:        config.twitter.consumer_key,
  consumer_secret:     config.twitter.consumer_secret,
  access_token_key:    config.twitter.access_token_key,
  access_token_secret: config.twitter.access_token_secret
});


console.log( "Connecting to db..." );
var fanatixApi = new Fanatix( config.database.host, config.database.port, config.database.poolSize, function(){

  console.log( "Loading Twitter ID mapper..." );
  var idMapper = new TwitterIDMapper( fanatixApi, function(){

    console.log( "Loading attractions for " + config.eventgroups.length + " eventgroups..." );
    fanatixApi.getAttractionsByEventgroups( config.eventgroups, { 'twitter.accounts': true, 'eventgroups._id': true, name: true }, null, null, function( err, attractions ) {

       console.log( attractions.length + " attractions loaded" );

       var accountMap = {};

       attractions.forEach( function( attraction ){

         if( ! attraction.twitter || ! attraction.twitter.accounts ) return true;

         var aid = idMapper.getAccountId( attraction.twitter.accounts[ 0 ] );
	
         if( ! aid ) return true;

         accountMap[ aid ] = {
           id:          attraction._id.toString(),
           name:        attraction.name,
           twitter:     attraction.twitter.accounts[ 0 ],
           eventgroups: attraction.eventgroups ? attraction.eventgroups.map(function(eventgroup){ return eventgroup._id.toString(); }) : null, 
         };
       }); 

       console.log( 'Streaming from ' + Object.keys( accountMap ).length + ' mapped twitter accounts' );

       var totalTweets    = 0,
           importedTweets = 0;

       twitterApi.stream(
         'statuses/filter',
         { 'follow': Object.keys( accountMap ) },
         function(stream) {
         stream.on('data', function(tweet) {

           totalTweets++;

           if( totalTweets % 1000 == 0 )
             console.log( "Stats: " + totalTweets + " total, " + importedTweets + " imported [" + ( Math.round( ( importedTweets / totalTweets ) * 100 ) / 100 ).toFixed( 2 ) + "%]" );
           
           if( tweet.in_reply_to_status_id || tweet.in_reply_to_user_id ) return true;

           var attraction = accountMap[ tweet.user.id ];
           if( ! attraction ) return true;

           var extId = 'twitter-' + tweet.id;

	   // The date format has changed since v1 API - for some reason, seems apps depend on this!
	   var dateWords = tweet.created_at.split( ' ' );
           var created_at = [ dateWords[ 0 ] + ',', dateWords[ 2 ], dateWords[ 1 ], dateWords[ 5 ], dateWords[ 3 ], dateWords[ 4 ] ].join( ' ' );

           var payload = {
             id: tweet.id,
             id_str: tweet.id_str,
             from_user: tweet.user.screen_name,
             from_user_id: tweet.user.id,
             from_user_id_str: tweet.user.id_str,
             from_user_name: tweet.user.name,
             profile_image_url: tweet.user.profile_image_url,
             text: tweet.text,
             created_at: created_at,
             entities: tweet.entities
           }; 

	   var postData = {
             source: 'tweet',
             country: 'gb',
             language: 'en',
             payload: JSON.stringify( payload ),
             attractions: [ attraction.id ],
             eventgroups: attraction.eventgroups
           };


           Restler.post( 'http://' + config.news.host + ':' + config.news.port + '/fanatix-news/v1.0/item/' + extId, {data: postData} ).on('complete', function(data, response) {
	     if( data && data.status == 'ok' && data.response )
             {
               console.log( data.response.id + " :: " + tweet.user.name + " :: " +  tweet.text );
               importedTweets++;
             }
           });

         });
       });
    });
  });
});

